var three = require("three");
var constants = require("./common/constants.js");

function EditModeHandler(){

	var state = constants.EDIT_STATE_SELECT;
	var selectedObjects = [];
	var textDiv = null;

	var setTextDiv = function(div){
		textDiv = div;
		console.log(textDiv.innerHTML);
	};


	var changeState = function(_state){

		state = _state;

		if(state === constants.EDIT_STATE_SELECT){
			textDiv.innerHTML = "SELECT AN ELEMENT";
		}
		
	};


	var addSelection = function(obj){

		selectedObjects.push(obj);

		// Change back to selected color
// 		obj.material.color = bla

	};


	var deselectAll = function(){

		for(o in selectedObjects){

			var obj = selectedObjects[o];

			// Change back to normal color
// 			obj.material.color = bla

		}

		selectedObjects = [];

	};


	var moveObjects = function(direction){

		for(o in selectedObjects){

			var obj = selectedObjects[o];

			if(obj.userData.type === constants.TYPE_LINE){
				lineFactory.geometry.vertices[0].position.add(direction);
				lineFactory.geometry.vertices[1].position.add(direction);
			} else if(obj.userData.type === constants.TYPE_ADDRESS){
				selectedObjects[0].position.add(direction);
			}

		}

	};


	var deleteObjects = function(){

		while(selectedObjects.length > 0){

			var obj = selectedObjects[0];

			if(obj.userData.type === constants.TYPE_LINE){
				lineFactory.deleteLine(obj);
			} else if(obj.userData.type === constants.TYPE_ADDRESS){
				addressFactory.deleteAddress(obj);
			}

			selectedObjects.shift();

		}

	};

	return {
		setTextDiv: setTextDiv,
		addSelection: addSelection,
		changeState: changeState,
		deselectAll: deselectAll,
		moveObjects: moveObjects,
		deleteObjects: deleteObjects
	};
};

module.exports = EditModeHandler;
