var three = require("three");
var constants = require("./common/constants.js");

function BackgroundImageFactory(){

	var tileMaterial = new three.MeshBasicMaterial({
		vertexColors: three.VertexColors
	});

	return {

		setTileMaterial: function(tileTexture){

			tileMaterial = new three.MeshBasicMaterial({
				map: tileTexture,
				vertexColors: three.VertexColors,
				transparent: true,
				side: three.DoubleSide
			});

		},

		createTileAtPosition: function(x, y, z, scene){

			var geometry = new three.PlaneGeometry(2048, 2048);
			var mesh = new three.Mesh(geometry, tileMaterial);
			mesh.position.set(x, y, z);
			mesh.scale.set(2.5, 2.5, 0.0);
 
			mesh.userData.gameObjectType = constants.TYPE_TILE;
			scene.add(mesh);

			return mesh;
		}
	};
};

module.exports = BackgroundImageFactory;
