Constants = {

	// DEVICE TYPES
	DEVICE_TYPE_DESKTOP: 0,
	DEVICE_TYPE_MOBILE: 1,
	DEVICE_TYPE_TABLET: 2,

	// RESOLUTION
	// iphone resolution
	// SCREEN_WIDTH_MAX: 375.0,
	// SCREEN_HEIGHT_MAX: 667.0,

	// Browser
	SCREEN_WIDTH_MAX: 800.0,
	SCREEN_HEIGHT_MAX: 600.0,

	// EDIT STATES
	EDIT_STATE_SELECT: 0,
	EDIT_STATE_DRAW_LINE: 1,
	EDIT_STATE_DELETE_LINE: 2,
	EDIT_STATE_CREATE_ADDRESS: 3,
	EDIT_STATE_DELETE_ADDRESS: 4,

	// GAME OBJECT TYPES
	TYPE_TILE: 0,
	TYPE_SPIDER: 1,
	TYPE_NAVCUBE: 2,
	TYPE_ITEM: 3,
	TYPE_PROJECTILE: 4,
	TYPE_ADDRESS: 5,

	// SPIDERS
	NUM_SPIDERS: 8,

	// ITEMS
	NUM_ITEMS: 16,

	// PROJECTILES
	NUM_PROJECTILES: 32,

	// MOUSE
	MOUSE_WHEEL_SENSITIVITY: 0.0001,
	MOUSE_ROTATE_SENSITIVITY: 0.006,

	// MAP
	GRID_SIZE_X: 14.0,
	GRID_SIZE_Y: 4.0,
	GRID_SIZE_Z: 14.0,
	BLOCK_SIZE: 100.0,

	// CAMERA
	NORMAL_ZOOM: 0.40,
	EXTRA_ZOOM: 0.80,

	// LAYERS
	LAYER_MAIN: 1,
	LAYER_COLLISION_ELEMENTS: 2,

	// FOG
	FOG_DENSITY: 1.5,

	// COLORS
// 	SKY_COLOR: 0xff9933, // Orange
// 	SKY_COLOR: 0xdaaae4, // Purple
	SKY_COLOR: 0xb574c3, // Purple
// 	SKY_COLOR: 0x87CEFA, // Sky blue

	BROWN_1: 0x663300,
	BROWN_2: 0x4d2600,
	BROWN_3: 0x331a00,
	BROWN_4: 0x1a0d00,
	BROWN_5: 0x010100,

	GREEN_1: 0x00ff00,
	GREEN_2: 0x00aa00,
	GREEN_3: 0x006600,
	GREEN_4: 0x004400,
	GREEN_5: 0x002200,

	// GREY
	GREY_1: 0xffffff,
	GREY_2: 0xaaaaaa,
	GREY_3: 0x666666,
	GREY_4: 0x444444,
	GREY_5: 0x222222,

	// PHYSICS
	LINE_SNAP_INCREMENT: 50.0,
	GRAVITY: 4.0,
	CHARACTER_FRICTION: 0.75,
	CHARACTER_HORIZONTAL_FORCE: 2.0,
	CHARACTER_DASH_FORCE: 4.0,
	CHARACTER_JUMP_FORCE: 50.0,
	CHARACTER_HORIZONTAL_WALL_JUMP_FORCE: 50.0,
	CHARACTER_WIDTH: 37.5,
	CHARACTER_HALF_WIDTH: 18.5,
	CHARACTER_HEIGHT: 75.0,
	CHARACTER_HALF_HEIGHT: 37.5,


	// ACTION IDS
	ACTION_ID_CONNECT: 0,
	ACTION_ID_SNAPSHOT: 1,
	ACTION_ID_JUMP: 2,
	ACTION_ID_FIRE: 3,
	ACTION_ID_MOVE: 4,
	ACTION_ID_FALL_THROUGH_FLOOR: 5,

	//COMMAND IDS
	INPUT_COMMAND_LEFT: 0,
	INPUT_COMMAND_RIGHT: 1,
	INPUT_COMMAND_UP: 2,
	INPUT_COMMAND_DOWN: 3,
	INPUT_COMMAND_JUMP: 4,
	INPUT_COMMAND_DASH: 5,
	INPUT_COMMAND_DEBUG: 6,

	// COLLISION LINE TYPES
	LINE_TYPE_HARD_FLOOR: 0,
	LINE_TYPE_SOFT_FLOOR: 1,
	LINE_TYPE_WALL: 2,

	// STATUS CODES
	STATUS_CODE_SUCCESS: 1,
	STATUS_CODE_FAIL: 2

}


module.exports = Constants;
