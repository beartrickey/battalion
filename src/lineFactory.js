var three = require("three");
var util = require("util");
var lineData = require("./lineData.js");
var constants = require("./common/constants.js");

LineFactory = function(){

	var lines = [];

	var wallMaterial = new three.LineBasicMaterial({
		color: 0x0000ff
	});

	var hardFloorMaterial = new three.LineBasicMaterial({
		color: 0x000000
	});

	var softFloorMaterial = new three.LineBasicMaterial({
		color: 0xffffff
	});

	var normalMaterial = new three.LineBasicMaterial({
		color: 0xff0000
	});

	var getSurfaceTangent = function(a, b){

		// Always make perpendicular with leftmost vertex first
		var leftVertex = a;
		var rightVertex = b;
		if(Math.min(a.x, b.x) === b.x){
			leftVertex = b;
			rightVertex = a;
		}
		return leftVertex.clone().sub(
			rightVertex
		).normalize();

	};

	var calculateNormal = function(line){
		line.userData.normal = new three.Vector3(
			-line.userData.tangent.y,
			line.userData.tangent.x,
			0.0
		);
	};

	var flipLineNormal = function(scene, line){
		line.userData.normal.multiply(
			new three.Vector3(-1.0, -1.0, 0.0)
		);

		scene.remove(line.userData.normalLine);
		drawNormal(scene, line);
	};

	var drawNormal = function(scene, line){
		
		var geometry = new three.Geometry();
		var p1 = line.geometry.vertices[0];
		var p2 = line.geometry.vertices[1];
		var a = p1.clone().sub(
			p1.clone().sub(p2).multiplyScalar(0.5)
		);
		var b = a.clone().add(line.userData.normal.clone().multiplyScalar(50));
		geometry.vertices.push(a, b);
		
		var normalLine = new three.Line(geometry, normalMaterial);
		normalLine.visible = false;
		line.userData.normalLine = normalLine;
		scene.add(normalLine);
	};

	var createLine = function(scene, lineType, p1, p2, normal){

		scene = scene;

		var geometry = new three.Geometry();
		geometry.vertices.push(p1, p2);

		// Precalculate if its a wall
		if(p1.x === p2.x){
			lineType = constants.LINE_TYPE_WALL;
		}

		// Change color based on surface type
		var material = hardFloorMaterial;
		if(lineType === constants.LINE_TYPE_SOFT_FLOOR){
			material = softFloorMaterial;
		}else if(lineType === constants.LINE_TYPE_WALL){
			material = wallMaterial;
		}
	
		var line = new three.Line(geometry, material);
		line.matrixAutoUpdate = false;
		lines.push(line);
		line.userData.leftLimit = Math.min(p1.x, p2.x);
		line.userData.rightLimit = Math.max(p1.x, p2.x);
		line.userData.tangent = getSurfaceTangent(p1, p2);
		line.userData.collision = true;
		line.userData.lineType = lineType;
		line.layers.set(constants.LAYER_COLLISION_ELEMENTS);
		scene.add(line);
		
		// Normal
		if(normal === null){
			calculateNormal(line);
		}else{
			line.userData.normal = normal;
		}
		drawNormal(scene, line, p1, p2);

		return line;
	};

	return {
		createLines: function(scene){

			var data = lineData.data;

			// Load lines from data here
			for(line in data){
				var p1 = new three.Vector3(
					data[line][1][0],
					data[line][1][1],
					data[line][1][2]
				);
				var p2 = new three.Vector3(
					data[line][2][0],
					data[line][2][1],
					data[line][2][2]
				);
				var normal = new three.Vector3(
					data[line][3][0],
					data[line][3][1],
					data[line][3][2]
				);
				createLine(scene, data[line][0], p1, p2, normal);
			}
		},

		flipLineNormal: flipLineNormal,

		deleteLine: function(scene, line){
			scene.remove(line.userData.normalLine);
			scene.remove(line);
			for(var i = 0; i < lines.length; i++){
				if(lines[i] === line){
					lines.splice(i, 1);
					return;
				}
			}
		},

		drawLines: function(){
			var data = [];
			for(l in lines){
				data.push([
					lines[l].userData.lineType,
					[
						lines[l].geometry.vertices[0].x,
						lines[l].geometry.vertices[0].y,
						lines[l].geometry.vertices[0].z
					],
					[
						lines[l].geometry.vertices[1].x,
						lines[l].geometry.vertices[1].y,
						lines[l].geometry.vertices[1].z
					],
					[
						lines[l].userData.normal.x,
						lines[l].userData.normal.y,
						0.0
					]
				]);
			}
			return data;
		},

		createLine: createLine,

		lines: lines
	};
};

module.exports = LineFactory;

