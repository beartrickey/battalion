var Keyboard = require("./keyboard.js");
var keyboard = new Keyboard();
var constants = require("./common/constants.js");

function KeyboardMapper(){

	var left = constants.INPUT_COMMAND_LEFT;

	var keyMap = {};

  keyMap[constants.INPUT_COMMAND_LEFT] = "65";
	keyMap[constants.INPUT_COMMAND_RIGHT] = "68";
	keyMap[constants.INPUT_COMMAND_UP] = "87";
	keyMap[constants.INPUT_COMMAND_DOWN] = "83";
	keyMap[constants.INPUT_COMMAND_JUMP] = "32";
	keyMap[constants.INPUT_COMMAND_DASH] = "16";


	var getState = function(commandInput){
	//check the command input against the key that is pressed

	var actualKey = keyMap[commandInput];
	var actualKeyState = keyboard.getKey(actualKey);

		return actualKeyState;

	};

	return {
		getState: getState
	}

}

module.exports = KeyboardMapper;
