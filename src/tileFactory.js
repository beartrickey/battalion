var three = require("three");
var constants = require("./common/constants.js");

function TileFactory(){

	var tiles = {};

	var tileMaterial = new three.MeshBasicMaterial({
		vertexColors: three.VertexColors
	});

	var GRID_X_MIN = (constants.GRID_SIZE_X - 1.0) / -2.0;
	var GRID_X_MAX = (constants.GRID_SIZE_X - 1.0) / 2.0;
	var GRID_Z_MIN = (constants.GRID_SIZE_Z - 1.0) / -2.0;
	var GRID_Z_MAX = (constants.GRID_SIZE_Z - 1.0) / 2.0;
	var shadeAmount = -0.15;

	return {

		setTileMaterial: function(tileTexture){

			tileMaterial = new three.MeshBasicMaterial({
				map: tileTexture,
				vertexColors: three.VertexColors
			});

		},


		getFacesWithNormal: function(normal){

			console.log(normal);

			for(t in tiles){
				var tile = tiles[t];
				for(f in tile.geometry.faces){
					var face = tile.geometry.faces[f];
					console.log(face);
					if(face.normal.equals(normal)){
						for(v in face.vertexColors){face.vertexColors[v].setRGB(0.0, 0.0, 1.0)}
					}
				}
				tile.geometry.colorsNeedUpdate = true;
			}

		},


		getRandomTileAndFace: function(){

			// Returns a random tile and face that a spider can be on.
			// Face can't be bottom or facing into existing tile
			var tileKeys = Object.keys(tiles);
			
			while(true){

				// Get random tile
				var randTileIndex = Math.floor(Math.random() * tileKeys.length);
				var tileKey = tileKeys[randTileIndex];
				var tile = tiles[tileKey];
				
				// Get random face
				var face = null;
				while(true){
					var f = Math.floor(Math.random() * 12);

					// HACK: Make sure its the upward facing face
					if(tile.geometry.faces[f].normal.y == 1){
						face = tile.geometry.faces[f];
						break;
					}
				}

				// Make sure there isn't a blocking tile
				var blockingTilePosX = tile.userData.gridCoordinates.x;
				var blockingTilePosY = face.normal.y + tile.userData.gridCoordinates.y;
				var blockingTilePosZ = tile.userData.gridCoordinates.z;
				if(!this.getTileAtPosition(blockingTilePosX, blockingTilePosY, blockingTilePosZ)){
					return [tile, face];
				}
			}
		},


		getTileAtPosition: function(x, y, z){
			return tiles[x + "," + y + "," + z];
		},

		getTileWithKey: function(key){
			return tiles[key];
		},

		createTileAtPosition: function(x, y, z, scene){
			console.log("createTileAtPosition()");
			console.log([x, y, z, scene]);

			var geometry = new three.BoxGeometry(constants.BLOCK_SIZE, constants.BLOCK_SIZE, constants.BLOCK_SIZE);
			var mesh = new three.Mesh(geometry, tileMaterial);
			mesh.translateX(x * constants.BLOCK_SIZE);
			mesh.translateY(y * constants.BLOCK_SIZE);
			mesh.translateZ(z * constants.BLOCK_SIZE);
 
			mesh.userData.gameObjectType = constants.TYPE_TILE;
			mesh.userData.collision = true;
			mesh.userData.gridCoordinates = new three.Vector3(x, y, z);;
			mesh.userData.tileKey = this.createTileKey(mesh);
			mesh.userData.stackDict = {};
 
			scene.add(mesh);

			tiles[mesh.userData.tileKey] = mesh;
			return mesh;
		},

		createTileKey: function(tile){
			return "" + tile.userData.gridCoordinates.x + "," + tile.userData.gridCoordinates.y + "," + tile.userData.gridCoordinates.z;
		},


		getTilePositionBuffer: function(){
			
			var returnBuffer = [];
			for(t in tiles){
				returnBuffer.push([
					tiles[t].userData.gridCoordinates.x,
					tiles[t].userData.gridCoordinates.y,
					tiles[t].userData.gridCoordinates.z
				]);
			}
			return returnBuffer;

		},


		createTilesFromBuffer: function(buffer, scene){

			for(t in buffer){
				this.createTileAtPosition(
					buffer[t][0],
					buffer[t][1],
					buffer[t][2],
					scene
				)
			}

		},


		shadeTile: function(tile){

			var shadedVerts = {};
			for(var v = 0; v < tile.geometry.vertices.length; v++){
			shadedVerts[v] = 0;
			var vert = tile.geometry.vertices[v];
			var vAbsolutePos = tile.position.clone().add(vert);
			for(var x = -1; x < 2; x++){
			for(var y = -1; y < 2; y++){
			for(var z = -1; z < 2; z++){

				var distance = 0;
				if(x != 0){distance += 1;}
				if(y != 0){distance += 1;}
				if(z != 0){distance += 1;}
				if(distance == 0){continue;}

				var absolutePosX = tile.userData.gridCoordinates.x + x;
				var absolutePosY = tile.userData.gridCoordinates.y + y;
				var absolutePosZ = tile.userData.gridCoordinates.z + z;

				var bTile = this.getTileAtPosition(absolutePosX, absolutePosY, absolutePosZ);
				if(bTile){
					for(var bv = 0; bv < bTile.geometry.vertices.length; bv++){
						var bVert = bTile.geometry.vertices[bv];
						var bvAbsolutePos = bTile.position.clone().add(bVert);
						if(vAbsolutePos.equals(bvAbsolutePos)){
							shadedVerts[v] += 1;
						}
					}
				}
			}}}}

			// Color each vert on each face
			for(var v in shadedVerts){
				var vInt = parseInt(v);
				for(var f = 0; f < tile.geometry.faces.length; f++){
					var vShadeAmount = shadedVerts[v] * shadeAmount;
					var face = tile.geometry.faces[f];
					if(face.a == vInt){face.vertexColors[0].addScalar(vShadeAmount);}
					if(face.b == vInt){face.vertexColors[1].addScalar(vShadeAmount);}
					if(face.c == vInt){face.vertexColors[2].addScalar(vShadeAmount);}
				}
			}
		},


		createTiles: function(scene){

			for(var row = GRID_X_MIN; row < GRID_X_MAX + 1; row++){

				for(var column = GRID_Z_MIN; column < GRID_Z_MAX + 1; column++){

					var heightUnits = Math.ceil(Math.random() * constants.GRID_SIZE_Y)
					
					// Outside edge is flat
					if(row === GRID_X_MIN || row === GRID_X_MAX || column === GRID_Z_MIN || column === GRID_Z_MAX){
						heightUnits = 1.0;
					}

					for(var h = 0; h < heightUnits; h++){
						this.createTileAtPosition(row, h, column, scene);
					}
				}
			}
		},


		colorMesh: function(mesh){

			var eastColor= constants.GREY_2;
			var westColor= constants.GREY_4;
			var topColor = constants.GREY_1;
			var bottomColor = constants.GREY_5;
			var northColor= constants.GREY_3;
			var southColor= constants.GREY_3;

			// Shade block based on facing
			for(f in mesh.geometry.faces){
				var face = mesh.geometry.faces[f];
				var color = eastColor;

				if(face.normal.x == 1){color = eastColor;}
				else if(face.normal.x == -1){color = westColor;}
				else if(face.normal.y == 1){color = topColor;}
				else if(face.normal.y == -1){color = bottomColor;}
				else if(face.normal.z == 1){color = northColor;}
				else if(face.normal.z == -1){color = southColor;}
				
				for(v = 0; v < 3; v++){
					if(face.vertexColors[v]){face.vertexColors[v].setHex(color);}
					else{face.vertexColors[v] = new three.Color(color);}
				}
			}
		},


		highlightFace: function(mesh, color){

			// Shade block based on facing
			for(f in mesh.geometry.faces){
				var face = mesh.geometry.faces[f];

				if(face.normal.y != 1){continue;}
				
				for(v = 0; v < 3; v++){
					if(face.vertexColors[v]){face.vertexColors[v].setHex(color);}
					else{face.vertexColors[v] = new three.Color(color);}
				}
			}
		},

		confirmTargetableTile: function(origin, tile){

			var distanceX = Math.abs(tile.userData.gridCoordinates.x - origin.x);
			var distanceY = Math.abs(tile.userData.gridCoordinates.y - origin.y);
			var differenceY = tile.userData.gridCoordinates.y - origin.y;
			var distanceZ = Math.abs(tile.userData.gridCoordinates.z - origin.z);

			var maxRange = 6.0;

			// Players can shoot further the higher up they are
			// (If the player is on a tile higher than the one we are evaluating, it is more likely they can hit it)
			if(differenceY < 0.0){
				// Player is higher than tile
				var rangeIncrease = Math.ceil(differenceY * -0.5);
				maxRange += rangeIncrease;
				var distance = distanceX + distanceZ;
				if(distance > maxRange){return false;}
			}else{
				// Player is at same height or lower than tile
				var distance = distanceX + distanceY + distanceZ;
				if(distance > maxRange){return false;}
			}

			return true;

		},

		highlightTargetableTiles: function(origin, color){

			this.shadeTiles();

			for(var t in tiles){

				var tile = tiles[t];
				if(this.confirmTargetableTile(origin, tile)){
					this.highlightFace(tile, color);
					tile.geometry.colorsNeedUpdate = true;
				}
			}
		},


		highlightMoveableTiles: function(origin, color){

			this.shadeTiles();

			for(var t in tiles){

				var tile = tiles[t];
				
				var distanceX = Math.abs(tile.userData.gridCoordinates.x - origin.x);
				var distanceY = Math.abs(tile.userData.gridCoordinates.y - origin.y);
				var distanceZ = Math.abs(tile.userData.gridCoordinates.z - origin.z);

				// Add to DistanceY for every object on this tile
				distanceY += Object.keys(tile.userData.stackDict).length;

				var distance = distanceX + distanceY + distanceZ;
				if(distance > 3.0){continue;}
				if(distanceX > 2.0){continue;}
				if(distanceZ > 2.0){continue;}
				this.highlightFace(tile, color);
				tile.geometry.colorsNeedUpdate = true;

			}
		},


		shadeTiles: function(){

			for(var t in tiles){

				var tile = tiles[t];
				
				this.colorMesh(tile);
				this.shadeTile(tile);
				tile.geometry.colorsNeedUpdate = true;

			}
		}
	};
};

module.exports = TileFactory;
