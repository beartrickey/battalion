//====================================================================================================
// IMPORTS
//====================================================================================================


// Node
var util = require("util");
var url = require("url");

// Third-party
var async = require("async");
var three = require("three");

// App
var constants = require("./common/constants.js");

var Keyboard = require("./keyboard.js");
var keyboard = new Keyboard();

var KeyboardMapper = require("./keyboardMapper.js");
var keyboardMapper = new KeyboardMapper();

var Gamepad = require("./gamepad.js");
var gamepad = new Gamepad();

var SpiderFactory = require("./spiderFactory.js");
var spiderFactory = SpiderFactory();

var BackgroundImageFactory = require("./backgroundImageFactory.js");
var backgroundImageFactory = BackgroundImageFactory();

var LineFactory = require("./lineFactory.js");
var lineFactory = LineFactory();

var AddressFactory = require("./addressFactory.js");
var addressFactory = AddressFactory();

var EditModeHandler = require("./editModeHandler.js");
var editModeHandler = new EditModeHandler();


//====================================================================================================
// DEVICE DETECTION
//====================================================================================================


var device = constants.DEVICE_TYPE_DESKTOP;
function detectDevice(){
	if(navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i)){
		device = constants.DEVICE_TYPE_MOBILE;
	}else if(navigator.userAgent.match(/iPad/i)){
		device = constants.DEVICE_TYPE_TABLET;
	}
}


//====================================================================================================
// DOM ELEMENTS
//====================================================================================================


var domMainContainer = null;
var domMainContainerRect = null;
var screenWidth = 0.0;
var screenHeight = 0.0;

function setupDom(){

	// Figure out screen resolution
	screenWidth = document.body.clientWidth;;
	screenHeight = document.body.clientHeight;;

	if(screenWidth > constants.SCREEN_WIDTH_MAX){screenWidth = constants.SCREEN_WIDTH_MAX;}
	if(screenHeight > constants.SCREEN_HEIGHT_MAX){screenHeight = constants.SCREEN_HEIGHT_MAX;}

	console.log("document.body size");
	console.log([document.body.clientWidth, document.body.clientHeight]);
	console.log("clamped screen size");
	console.log([screenWidth, screenHeight]);

	// Set main-container size
	domMainContainer = document.getElementById("main-container");
	domMainContainer.style.width = screenWidth + "px";
	domMainContainer.style.height = screenHeight + "px";
	domMainContainer.style.minWidth =  screenWidth + "px";
	domMainContainer.style.minHeight = screenHeight + "px";
	domMainContainer.style.maxWidth = screenWidth + "px";
	domMainContainer.style.maxHeight = screenHeight + "px";

	// Set editMode
	editModeDiv = document.getElementById("edit-mode-text-div");
	var urlComponents = url.parse(window.location.href, true);
	var query = urlComponents.query;
	if(query["editMode"]){
		editMode = query["editMode"] === "true";
	}

	if(editMode){
// 		editModeHandler.changeState(constants.EDIT_STATE_SELECT);
// 		editModeHandler.setTextDiv(editModeDiv);
	}else{
		document.body.removeChild(editModeDiv);
	}

	arrangeDomElements();

}


function arrangeDomElements(){

	domMainContainerRect = domMainContainer.getBoundingClientRect();

// 	$("#crosshairs").css("position", "absolute").
// 	css("left", (domMainContainerRect.left + (screenWidth * 0.5) - 32) + "px").
// 	css("top", (domMainContainerRect.bottom - 96) + "px");

}


//====================================================================================================
// GAME VARIABLES
//====================================================================================================



//====================================================================================================
// MOUSE CONTROLS
//====================================================================================================


var lastMousePosition = new three.Vector2(0.0, 0.0);
var mouseIsDown = false;
var mouseMovedAfterDown = false;
var mouseDownCounter = 0;
var uiElementPrecedent = false;


function onRightMouseClick(e){

	e.preventDefault();

};


function onMouseMove(e){

	e.preventDefault();

	mouseMovedAfterDown = true;

	// Skip if mousemove event but mouse isn't down
	if(e.type === "mousemove" && mouseIsDown === false)
		return;

	var currentMousePosition = getMouseCoords(e);
	var xDif = lastMousePosition.x - currentMousePosition[0];
	var yDif = lastMousePosition.y - currentMousePosition[1];

	lastMousePosition = new three.Vector2(
		currentMousePosition[0],
		currentMousePosition[1]
	)

	// Scroll camera around
// 	cameraSwivelObjectBase.position.sub(new three.Vector3(-xDif, yDif, 0.0));

	// Rotate camera swivel
	cameraSwivelObjectX.rotation.x += yDif * constants.MOUSE_ROTATE_SENSITIVITY;
	cameraSwivelObjectY.rotation.y += xDif * constants.MOUSE_ROTATE_SENSITIVITY;
}


function onMouseDown(e){

	mouseIsDown = true;
	mouseMovedAfterDown = false;

	var currentMousePosition = getMouseCoords(e);
	lastMousePosition = new three.Vector2(
		currentMousePosition[0],
		currentMousePosition[1]
	)


	// Create Address
	if(keyboard.getKey("82")){
		addressFactory.createAddressAtPosition(currentMousePosition.x, currentMousePosition.y, 0.0, scene);
		return;
	}

	// Do stuff with line
	if(keyboard.getKey("82")){  // Remove collision line with R
		deleteCollisionLine();
	}else if(keyboard.getKey("70")){  // Flip normal with F
		flipLineNormal();
	}else{
		makeCollisionPoint();
	}

}


function onUiElementUp(e){

	console.log("onUiElementUp");
	uiElementPrecedent = true;

}


function onMouseUp(e){

	console.log("onMouseUp");

	// Only handle collision with 3D elements:
	// - If no ui elements were touched first
	// - If the player wasn't dragging/rotating the camera around
	if(uiElementPrecedent === false && mouseMovedAfterDown === false){

		handleTouchCollision();

	}

	mouseDownCounter = 0;
	mouseIsDown = false;
	uiElementPrecedent = false;
}


function onMouseWheel(e){

	// Commenting out zooming
// 	var wheelDelta = e.wheelDelta;
// 	wheelDelta *= constants.MOUSE_WHEEL_SENSITIVITY;
// 	camera.zoom += wheelDelta;
// 	camera.position.z += 10.0 * wheelDelta;
// 	camera.updateProjectionMatrix();

}


function getMouseCoords(e){

	var posx = 0;
	var posy = 0;

	if(e.pageX && e.pageY){
		posx = e.pageX;
		posy = e.pageY;
	}
	else if(e.originalEvent.targetTouches[0].pageX && e.originalEvent.targetTouches[0].pageY){
		posx = e.originalEvent.targetTouches[0].pageX;
		posy = e.originalEvent.targetTouches[0].pageY;
	}
	else if(e.clientX && e.clientY)
	{
		posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	else if(e.originalEvent.targetTouches[0].clientX && e.originalEvent.targetTouches[0].clientY)
	{
		posx = e.originalEvent.targetTouches[0].clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		posy = e.originalEvent.targetTouches[0].clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}

	return [posx, posy];
}


// ============================================
// GAMEPAD
// ============================================


function checkGamepad(){

	gamepad.poll();

	// Left d-pad
	if(gamepad.getButton(7)){
		// Send websocket message
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_MOVE,
				spiderUuid: connection.sessionId,
				direction: -1.0
			}
		}));
	}

	// Right d-pad
	if(gamepad.getButton(5)){
		// Send websocket message
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_MOVE,
				spiderUuid: connection.sessionId,
				direction: 1.0
			}
		}));
	}

	// X button
	if(gamepad.getPress(14) && !gamepad.getButton(6)){
		// Send websocket message
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_JUMP,
				spiderUuid: connection.sessionId
			}
		}));
	}

	// X button
	if(gamepad.getPress(14) && gamepad.getButton(6)){
		// Send websocket message
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_FALL_THROUGH_FLOOR,
				spiderUuid: connection.sessionId
			}
		}));
	}

	// Rotate camera swivel with analog sticks
// 	if(gamepad.getAxes(0)){
// 		cameraSwivelObjectX.rotation.x += gamepad.getAxes(1) * constants.MOUSE_ROTATE_SENSITIVITY;
// 		cameraSwivelObjectY.rotation.y += gamepad.getAxes(0) * constants.MOUSE_ROTATE_SENSITIVITY;
// 	}

	// Reset states every frame
	gamepad.flushPressReleaseState();

}


// ============================================
// KEYBOARD
// ============================================


function checkKeyboard(){

	// Left arrow or A

	//used to add Dash functionality
	var characterDirection;



	if(keyboardMapper.getState(constants.INPUT_COMMAND_LEFT)){
		// Send websocket message
		//-1 for walk normal -2 for dash.
		//set when shift key is set
		if(keyboardMapper.getState(constants.INPUT_COMMAND_DASH)){
			characterDirection = -2.0
		}else{
			characterDirection = -1.0
		}
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_MOVE,
				spiderUuid: connection.sessionId,
				direction: characterDirection
			}
		}));
	}

	// Right arrow or D
	if(keyboardMapper.getState(constants.INPUT_COMMAND_RIGHT)){
		// Send websocket message
		// Send websocket message
		//-1 for walk normal -2 for dash.
		//set when shift key is set
		if(keyboardMapper.getState(constants.INPUT_COMMAND_DASH)){
			characterDirection = 2.0
		}else{
			characterDirection = 1.0
		}
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_MOVE,
				spiderUuid: connection.sessionId,
				direction: characterDirection
			}
		}));
	}

	// Jump while holding down at the same time to fall through soft floors
	if(keyboardMapper.getState(constants.INPUT_COMMAND_DOWN) &&
		 keyboardMapper.getState(constants.INPUT_COMMAND_JUMP)){
		// Send websocket message
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_FALL_THROUGH_FLOOR,
				spiderUuid: connection.sessionId
			}
		}));
	}

	// Space
	if(keyboardMapper.getState(constants.INPUT_COMMAND_JUMP)){
		// Send websocket message
		connection.send(JSON.stringify({
			payload: {
				actionId: constants.ACTION_ID_JUMP,
				spiderUuid: connection.sessionId
			}
		}));
	}


	// EDIT FUNCTIONS

	// Show/Hide collision lines with H
	if(keyboardMapper.getState(constants.INPUT_COMMAND_DEBUG)){
		camera.layers.toggle(constants.LAYER_COLLISION_ELEMENTS);
	}

	// Reset states every frame
	keyboard.flushPressReleaseState();

}


// ====================================================================================================
// Textures
// ====================================================================================================


var mapTexture = null;
var keeperTexture = null;
function loadTextures(allTexturesLoadedCallback){

 	var tl = new three.TextureLoader();
	async.series(
		[
			function(callback){
				tl.load(
					"/textures/map_texture.png",
					function(texture){
						texture.wrapS = three.RepeatWrapping;
						texture.wrapT = three.RepeatWrapping;
						mapTexture = texture;

						callback(null, "map texture loaded");
					},
					// Function called when download progresses
					function(xhr){
					},
					// Function called when download errors
					function(xhr){
						console.log( 'An error happened' );
					}
				);
			},
			function(callback){
				tl.load(
					"/textures/keeper_tex.png",
					function(texture){
						texture.wrapS = three.RepeatWrapping;
						texture.wrapT = three.RepeatWrapping;
						keeperTexture = texture;

						callback(null, "keeper texture loaded");
					},
					// Function called when download progresses
					function(xhr){
					},
					// Function called when download errors
					function(xhr){
						console.log( 'An error happened' );
					}
				);
			},

		],
		function(err, result){

			allTexturesLoadedCallback(null, "textures loaded");

		}
	);

}


// ====================================================================================================
// Models
// ====================================================================================================


// Three json blender exporter: https://github.com/mrdoob/three.js/tree/master/utils/exporters/blender
// Exporter notes:
// Make sure model is at rest pose when exporting
// Use scale == 1.0 in exporter settings (instead scale in js code)

var keeperGeometry = null;
function loadModels(asyncCallback){

	var cl = new three.JSONLoader();
	cl.load(
 		"/models/keeper.js",
		function(geometry, materials){
			keeperGeometry = geometry;
			asyncCallback(null, "model loaded");
		},
		// Function called when download progresses
		function(xhr){
			console.log("on progress");
		},
		// Function called when download errors
		function(xhr){
			console.log("on error");
		}
	);

}


// ====================================================================================================
// Event Listeners
// ====================================================================================================


function addEventListeners(){

	// EventListeners
	document.body.addEventListener("mousewheel", onMouseWheel, false);
	window.addEventListener("resize", arrangeDomElements);

	if(device === constants.DEVICE_TYPE_DESKTOP){

		$("body").on("mousemove", onMouseMove);
		$("body").on("mousedown", onMouseDown);
		$("body").on("mouseup", onMouseUp);
		$("body").on("contextmenu", onRightMouseClick);

		$(".ui-element").on("mouseup", onUiElementUp);

// 		$("#crosshairs").get(0).addEventListener("mouseup", onCrosshairsUp);

	}else if(device === constants.DEVICE_TYPE_MOBILE || device === constants.DEVICE_TYPE_TABLET){

		$("body").on("touchstart", onMouseDown);
		$("body").on("touchmove", onMouseMove);
		$("body").on("touchend", onMouseUp);

		$(".ui-element").on("touchend", onUiElementUp);

// 		$("#crosshairs").get(0).addEventListener("touchend", onCrosshairsUp);
	}

}


// ====================================================================================================
// WebSocket Listeners
// ====================================================================================================


// TODO: Move vars to better place
var snapshotBuffer = [];
var clientTime = 0.0;
var connection = null;
function initializeWebSocketConnection(callback){

	// TODO: Add handling for connection timeout

	// Get server from query params if available
	var serverAddress = "localhost:8080";

	var urlComponents = url.parse(window.location.href, true);
	var query = urlComponents.query;
	if(query["serverAddress"]){
		serverAddress = query["serverAddress"];
	}

	// Contact server and retrieve map data
	connection = new WebSocket("ws://" + serverAddress);

	connection.onopen = function(){
		connection.send(JSON.stringify({
			"ping": "ping"
		}));
	};

	connection.onerror = function(error){
		console.log("WebSocket Error: " + error);
	};

	// Handle messages from the server
	connection.onmessage = function(message){
		try{
			// Parse client requests here
			data = JSON.parse(message.data);

			// On connect
			if(data.payload && data.payload.actionId == constants.ACTION_ID_CONNECT){

				console.log("connection successful");

				connection.sessionId = data.payload.sessionId;

				// Create map from server data

				// Don't callback until connection has been made and data retrieved
				callback(null, "Initialize WebSocket Connection");
			}

			// On snapshot
			if(data.payload && data.payload.actionId == constants.ACTION_ID_SNAPSHOT){

				// Add snapshot to end of buffer
				data.payload.clientTime = getTime();
				snapshotBuffer.push(data.payload);

				if(data.payload.spiderBuffer[connection.sessionId] && spiderFactory.spiderDictionary[connection.sessionId]){

					var spider = spiderFactory.spiderDictionary[connection.sessionId];

					// Change rotation based on movement direction
					var xDif = data.payload.spiderBuffer[connection.sessionId][0] - spider.position.x;
					if(xDif < 0.0){
						spider.rotation.y = 1.8;
					}else if (xDif > 0.0){
						spider.rotation.y = -1.8;
					}

					// Update position
					spider.position.set(
						data.payload.spiderBuffer[connection.sessionId][0],
						data.payload.spiderBuffer[connection.sessionId][1],
						data.payload.spiderBuffer[connection.sessionId][2]
					);

					// Play animation
					if(Math.abs(xDif) < 1.0){
						spider.userData.stopAnimation();
					}else{
						spider.userData.playAnimation();
					}
				}
			}

		}catch(err){
			console.log(util.inspect(err));
			callback(err, "Initialize WebSocket Connection");
		}

	};

}


// ============================================
// Three initialization
// ============================================


var camera = null;
var cameraSwivelObjectBase = new three.Object3D();
var cameraSwivelObjectY = new three.Object3D();
var cameraSwivelObjectX = new three.Object3D();
var scene = null;
var clock = new three.Clock();
var renderer = null;
var raycaster = new three.Raycaster();
var collisionPoints = [];
var lastTime = new Date().getTime();
var editMode = false;

$(document).ready(function(){

	// Controls
	keyboard.setDebug(true);

	async.series(
		[
			function(callback){
				// Detect device
				detectDevice();
				callback(null, "Detected device");
			},
			function(callback){
				// DOM initialization
				setupDom();
				callback(null, "Setup DOM");
			},
			function(callback){
				// Add event listeners
				addEventListeners();
				callback(null, "Add event listeners");
			},
			function(callback){
				// Initialize Scene
				initializeScene();
				callback(null, "Initialize scene");
			},
			function(callback){
				// Load textures
				loadTextures(callback);
			},
			function(callback){
				// Setup texture-dependent materials
				spiderFactory.setSpiderMaterial(keeperTexture);
				backgroundImageFactory.setTileMaterial(mapTexture);
				callback(null, "Initialize materials");
			},
			function(callback){
				// Load models
				loadModels(callback);
			},
			function(callback){
				// Setup geometry
				spiderFactory.setSpiderGeometry(keeperGeometry);
				callback(null, "Initialize geometry");
			},
			function(callback){
				spiderFactory.createSpiders(scene);
				backgroundImageFactory.createTileAtPosition(-650.0, 449.0, -1.0, scene);
				callback(null, "Create spiders");
			},
			function(callback){
				// Initialize WebSocket connection
				console.log(connection);
				initializeWebSocketConnection(callback);
			}

		],
		function(err, result){

			console.log(err);
			console.log(result);
			console.log(connection);
			animate();

		}
	);


});


function initializeScene(){

	camera = new three.OrthographicCamera(
		-(screenWidth / 2),
		(screenWidth / 2),
		(screenHeight / 2),
		-(screenHeight / 2),
		8000.0,
		12000.0
	);

	camera.position.z = 10000.0;
	camera.zoom = constants.NORMAL_ZOOM;;
	camera.updateProjectionMatrix();
	camera.layers.enable(constants.LAYER_MAIN);
	camera.layers.enable(constants.LAYER_COLLISION_ELEMENTS);

	// scene > cameraSwivelObjectBase > cameraSwivelObjectY > cameraSwivelObjectX > camera
	cameraSwivelObjectX.add(camera);
	cameraSwivelObjectY.add(cameraSwivelObjectX);
	cameraSwivelObjectBase.add(cameraSwivelObjectY);
// 	cameraSwivelObjectX.rotation.x = Math.PI * -0.15;
// 	cameraSwivelObjectY.rotation.y = Math.PI * 0.25
	cameraSwivelObjectBase.position.y = 100.0;
	initCameraPosition();

	scene = new three.Scene();
	scene.add(cameraSwivelObjectBase);
// 	scene.fog = new three.FogExp2(constants.SKY_COLOR, constants.FOG_DENSITY);
// 	document.body.style["background-color"] = "#" + constants.SKY_COLOR.toString(16);

	renderer = new three.WebGLRenderer();
	renderer.setClearColor(constants.SKY_COLOR);
	renderer.setSize(window.innerWidth, window.innerHeight);

	domMainContainer.appendChild(renderer.domElement);

	// Set renderer size (appears as "canvas" element in dom)
	$("canvas").css("width", screenWidth + "px");
	$("canvas").css("height", screenHeight + "px");
	$("canvas").css("min-width", screenWidth + "px");
	$("canvas").css("min-height", screenHeight + "px");
	$("canvas").css("max-width", screenWidth + "px");
	$("canvas").css("max-height", screenHeight + "px");

	// Create collision lines
	lineFactory.createLines(scene);

}


function initCameraPosition(){

	cameraSwivelObjectBase.position.set(0, 200, 0);
	camera.position.z = 10000.0;
	camera.zoom = constants.NORMAL_ZOOM;
	camera.near = 8000.0;
	camera.far = 12000;
	camera.updateProjectionMatrix();

}


function flipLineNormal(){

	var offsetMouseX = lastMousePosition.x - domMainContainerRect.left;
	var offsetMouseY = lastMousePosition.y - domMainContainerRect.top;

	var scaledMouseX = ((offsetMouseX / screenWidth) * 2.0) - 1.0;
	var scaledMouseY = (-(offsetMouseY / screenHeight) * 2.0) + 1.0;

	raycaster.setFromCamera(
		new three.Vector2(
			scaledMouseX,
			scaledMouseY
		),
		camera
	);
	raycaster.linePrecision = 10;

	var intersectList = raycaster.intersectObjects(scene.children, true);

	// intersectList is sorted with closest first
	while(intersectList.length > 0){
		var intersection = intersectList[0];

		// Skip if collision not enabled
		if(!intersection.object.userData.collision){
			intersectList.shift();
			continue;
		}

		lineFactory.flipLineNormal(scene, intersection.object);

		if(editMode){
			var data = lineFactory.drawLines();
			connection.send(JSON.stringify({
				payload: {
					actionId: constants.ACTION_ID_SAVE_LINES,
					lineData: data
				}
			}));
		}

		return;
	}

};


function deleteCollisionLine(){

	var offsetMouseX = lastMousePosition.x - domMainContainerRect.left;
	var offsetMouseY = lastMousePosition.y - domMainContainerRect.top;

	var scaledMouseX = ((offsetMouseX / screenWidth) * 2.0) - 1.0;
	var scaledMouseY = (-(offsetMouseY / screenHeight) * 2.0) + 1.0;

	raycaster.setFromCamera(
		new three.Vector2(
			scaledMouseX,
			scaledMouseY
		),
		camera
	);
	raycaster.linePrecision = 10;

	var intersectList = raycaster.intersectObjects(scene.children, true);

	// intersectList is sorted with closest first
	while(intersectList.length > 0){
		var intersection = intersectList[0];

		// Skip if collision not enabled
		if(!intersection.object.userData.collision){
			intersectList.shift();
			continue;
		}

		lineFactory.deleteLine(scene, intersection.object);
		return;
	}

};

function makeCollisionPoint(){

	var offsetMouseX = lastMousePosition.x - domMainContainerRect.left;
	var offsetMouseY = lastMousePosition.y - domMainContainerRect.top;

	var scaledMouseX = ((offsetMouseX / screenWidth) * 2.0) - 1.0;
	var scaledMouseY = (-(offsetMouseY / screenHeight) * 2.0) + 1.0;

	raycaster.setFromCamera(
		new three.Vector2(
			scaledMouseX,
			scaledMouseY
		),
		camera
	);

	// Snap point to grid
	var increment = constants.LINE_SNAP_INCREMENT;;
	var point = raycaster.ray.origin;
	var snappedPointX = Math.round(point.x / increment) * increment;
	var snappedPointY = Math.round(point.y / increment) * increment;
	point = new three.Vector3(
		snappedPointX,
		snappedPointY,
		0.0
	);

	// Store points
	collisionPoints.push(point);

	// Draw actual points and lines
	var lineType = constants.LINE_TYPE_HARD_FLOOR;
	if(keyboard.getKey("83")){
		lineType = constants.LINE_TYPE_SOFT_FLOOR;
	}

	if(collisionPoints.length % 2 === 0){
		lineFactory.createLine(
			scene,
			lineType,
			collisionPoints[collisionPoints.length-1],
			collisionPoints[collisionPoints.length-2],
			null
		)

		// Send data to server if edit mode is on
		if(editMode){
			var data = lineFactory.drawLines();
			connection.send(JSON.stringify({
				payload: {
					actionId: constants.ACTION_ID_SAVE_LINES,
					lineData: data
				}
			}));
		}
	}
}

function handleTouchCollision(){

	var offsetMouseX = lastMousePosition.x - domMainContainerRect.left;
	var offsetMouseY = lastMousePosition.y - domMainContainerRect.top;

	var scaledMouseX = ((offsetMouseX / screenWidth) * 2.0) - 1.0;
	var scaledMouseY = (-(offsetMouseY / screenHeight) * 2.0) + 1.0;

	raycaster.setFromCamera(
		new three.Vector2(
			scaledMouseX,
			scaledMouseY
		),
		camera
	);

	// Calculate objects intersecting the picking ray
	var intersectList = raycaster.intersectObjects(scene.children, true);

	// intersectList is sorted with closest first
	while(intersectList.length > 0){
		var intersection = intersectList[0];

		// Skip if collision not enabled
		if(!intersection.object.userData.collision){
			intersectList.shift();
			continue;
		}

		// Pop this intersection off if no action is to be taken
		intersectList.shift();

	}

}


function animate(){

	// Check keyboard
	checkKeyboard();

	// Check keyboard
	checkGamepad();

	// Check touch
	if(mouseIsDown === true){
		mouseDownCounter++;
	}

	// Update clientTime
	clientTime = getTime();

	// Render game state 100 milliseconds in the past
	var renderTime = clientTime - 100;

	// Which snapshots is this time between?
	var snapshotA = null;
	var snapshotB = null;
	var a = 0;
	for(s in snapshotBuffer){
		var snapshot = snapshotBuffer[s];
		if(renderTime > snapshot.clientTime){
			snapshotA = snapshot;
			snapshotAIndex = s;
		}
	}

	// Perform actions if we've found a valid snapshot
	if(snapshotA){
		try{
			// See if valid snapshotB exists
			snapshotB = snapshotBuffer[a + 1];
			var snapshotTimeDifference = snapshotB.clientTime - snapshotA.clientTime;
			var offset = renderTime - snapshotA.clientTime;
			var scaledTime = offset / snapshotTimeDifference;

			var spiderBufferA = snapshotA.spiderBuffer;
			var spiderBufferB = snapshotB.spiderBuffer;

			// Delete local spiders that weren't referenced in the snapshot
			for(var uuid in spiderFactory.spiderDictionary){
				if(!spiderBufferA[uuid]){
					spiderFactory.spiderDictionary[uuid].userData.deactivate();
				}
			}

			// Interpolate spiders
			for(var uuid in spiderBufferA){

				if(spiderBufferA[uuid] && spiderBufferB[uuid]){

					// Create new spider if necessary
					var spider = spiderFactory.spiderDictionary[uuid];
					if(!spider){
						spider = spiderFactory.getFreeSpider();
						console.log("creating new spider");

						// Do something if this is the user's spider
						spider.userData.activate(uuid);
// 						if(spider.userData.uuid === connection.sessionId){
// 						}
					}

					// Don't interpolate this client's character (improves responsiveness)
					if(spider.userData.uuid === connection.sessionId){
						continue;
					}


					// Interpolate position
					var posA = new three.Vector3(
						spiderBufferA[uuid][0],
						spiderBufferA[uuid][1],
						spiderBufferA[uuid][2]
					);

					var posB = new three.Vector3(
						spiderBufferB[uuid][0],
						spiderBufferB[uuid][1],
						spiderBufferB[uuid][2]
					);

					var interpolatedPosition = posA.clone()
						.sub(posB)
						.multiplyScalar(-scaledTime)
						.add(posA);

					spider.position.copy(interpolatedPosition);

					// Change rotation based on movement direction
					var xDif = posB.x - posA.x;
					if(xDif < 0.0){
						spider.rotation.y = 1.8;
					}else if (xDif > 0.0){
						spider.rotation.y = -1.8;
					}

					// Play animation
					if(Math.abs(xDif) < 0.01){
						spider.userData.stopAnimation();
					}else{
						spider.userData.playAnimation();
					}

				}
			}

		}catch(err){
			// Catch out of range error
			// Error handling for when there aren't two snapshots to interpolate between
			// Disply lag icon?
			console.log("Error");
			console.log(err);
		}
	}

	// When is it OK to dispose of old snapshots?
	if(snapshotBuffer.length >= 5){
		snapshotBuffer.shift();
	}

	// Play mesh animations
	var dt = clock.getDelta();
	spiderFactory.playMeshAnimations(dt);

	// Scroll camera around based on player position
	var clientCharacter = spiderFactory.spiderDictionary[connection.sessionId];
	if(!editMode){
		if(clientCharacter){

			// TODO: Make these values % of screen size instead of absolute distances
			var cameraScrollThresholdX = 500.0;
			var cameraScrollThresholdY = 200.0;
			var rightLimit = cameraSwivelObjectBase.position.x + cameraScrollThresholdX;
			var leftLimit = cameraSwivelObjectBase.position.x - cameraScrollThresholdX;
			var topLimit = cameraSwivelObjectBase.position.y + cameraScrollThresholdY;
			var bottomLimit = cameraSwivelObjectBase.position.y - cameraScrollThresholdY;
			if(clientCharacter.position.x > rightLimit){
				var dif = clientCharacter.position.x - rightLimit;
				cameraSwivelObjectBase.position.setX(cameraSwivelObjectBase.position.x + dif);
			}
			if(clientCharacter.position.x < leftLimit){
				var dif = clientCharacter.position.x - leftLimit;
				cameraSwivelObjectBase.position.setX(cameraSwivelObjectBase.position.x + dif);
			}
			if(clientCharacter.position.y > topLimit){
				var dif = clientCharacter.position.y - topLimit;
				cameraSwivelObjectBase.position.setY(cameraSwivelObjectBase.position.y + dif);
			}
			if(clientCharacter.position.y < bottomLimit){
				var dif = clientCharacter.position.y - bottomLimit;
				cameraSwivelObjectBase.position.setY(cameraSwivelObjectBase.position.y + dif);
			}

		}
	}

	// note: three.js includes requestAnimationFrame shim
	requestAnimationFrame(animate);

	renderer.render(scene, camera);

}


// ====================================================================================================
// Helper functions
// ====================================================================================================


function getTime(){

	var date = new Date();
	return date.getTime();

}


// ====================================================================================================
// EOF
// ====================================================================================================
