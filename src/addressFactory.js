var three = require("three");
var constants = require("./common/constants.js");

function AddressFactory(){

	var addressMaterial = new three.MeshBasicMaterial({
		vertexColors: three.VertexColors
	});

	var blockSize = 100.0;

	return {

		createAddressAtPosition: function(x, y, z, scene){

			var geometry = new three.CircleGeometry(blockSize, 8);
			var mesh = new three.Mesh(geometry, addressMaterial);
			mesh.position.set(x, y, z);
 
			mesh.userData.gameObjectType = constants.TYPE_ADDRESS;
			mesh.userData.collision = true;
			mesh.layers.set(constants.LAYER_COLLISION_ELEMENTS);
 
			scene.add(mesh);

			return mesh;
		},

		createTiles: function(scene){

			for(var row = GRID_X_MIN; row < GRID_X_MAX + 1; row++){

				for(var column = GRID_Z_MIN; column < GRID_Z_MAX + 1; column++){

					var heightUnits = Math.ceil(Math.random() * constants.GRID_SIZE_Y)
					
					// Outside edge is flat
					if(row === GRID_X_MIN || row === GRID_X_MAX || column === GRID_Z_MIN || column === GRID_Z_MAX){
						heightUnits = 1.0;
					}

					for(var h = 0; h < heightUnits; h++){
						this.createTileAtPosition(row, h, column, scene);
					}
				}
			}
		}
	};
};

module.exports = AddressFactory;
