var Gamepad = require("./gamepad.js");
//var gamepad = new Gamepad();
var constants = require("./common/constants.js");

function GamepadMapper(){

	var left = constants.INPUT_COMMAND_LEFT;

	var GAMEPAD_INPUT_TYPE_BUTTON = 0;
	var GAMEPAD_INPUT_TYPE_AXES = 1;

  var gamepad = null;
	var padMap = {};

  padMap[constants.INPUT_COMMAND_LEFT] = [GAMEPAD_INPUT_TYPE_BUTTON, 4];
	padMap[constants.INPUT_COMMAND_RIGHT] = [GAMEPAD_INPUT_TYPE_BUTTON, 5];
	padMap[constants.INPUT_COMMAND_UP] = 	[GAMEPAD_INPUT_TYPE_BUTTON, 6];
	padMap[constants.INPUT_COMMAND_DOWN] = [GAMEPAD_INPUT_TYPE_BUTTON, 7];
	padMap[constants.INPUT_COMMAND_JUMP] = [GAMEPAD_INPUT_TYPE_BUTTON, 1];
	padMap[constants.INPUT_COMMAND_DASH] = [GAMEPAD_INPUT_TYPE_BUTTON, 3];
//	padMap[constants.INPUT_COMMAND_LEFT] = [GAMEPAD_INPUT_TYPE_AXES, 0.5]
//	padMap[constants.INPUT_COMMAND_RIGHT] = [GAMEPAD_INPUT_TYPE_AXES, -0.5]


	var getState = function(commandInput){
  //pass in the value from client.js will left, right, jump, etc
 //check the command input against the key that is pressed
//  console.log("these are the values you are passing in (CommandInput)")
//	console.log(commandInput);

  var mapping = padMap[commandInput]
  //will say if it is a axes or button type

	if(mapping[0] == GAMEPAD_INPUT_TYPE_BUTTON){
//		console.log(mapping[1]);
		return gamepad.getButton(mapping[1]);
	}

  if(mapping[0] == GAMEPAD_INPUT_TYPE_AXES){
	  return null;
	}

	};

	return {
		setGamepad: function(_gamepad){gamepad = _gamepad;},
		getState: getState
	}

}

module.exports = GamepadMapper;
