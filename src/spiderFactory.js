var three = require("three");
var constants = require("./common/constants.js");

SpiderFactory = function(){

	var spiders = [];
	var spiderDictionary = {};
	var width = 50.0;
	var height = 50.0;
	var raycaster = new three.Raycaster();
	var scene = null;

	var spiderMaterial = new three.MeshBasicMaterial({
		vertexColors: three.FaceColors
	});

	var spiderGeometry = new three.BoxGeometry(width, height, width);

	var createName = function(){

		var firstNames = [
			"Poindexter",
			"Harpalus",
			"Cato",
			"Eureka",
			"Yert"
		];

		var lastNames = [
			"The Third",
			"Jones",
			"Kanemoto",
			"Yamaha",
			"Nakata"
		];

		var randFirstIndex = Math.floor(Math.random() * firstNames.length);
		var randLastIndex = Math.floor(Math.random() * lastNames.length);
		var firstName = firstNames[randFirstIndex];
		var lastName = lastNames[randLastIndex];

		return firstName + " " + lastName;
	};

	var createSpider = function(scene){

		scene = scene;

		spiderMaterial.skinning = true;
		spiderMaterial.side = three.DoubleSide;
		var mesh = new three.SkinnedMesh(
			spiderGeometry,
			spiderMaterial
		);

		// Animation
		if(spiderGeometry.animations){
			mesh.userData.animationMixer = new three.AnimationMixer(mesh);

			for(var i = 0; i < spiderGeometry.animations.length; ++ i){
				mesh.userData.animationMixer.clipAction(spiderGeometry.animations[i]);
			}

			mesh.userData.currentAnimationAction = mesh.userData.animationMixer.clipAction(mesh.geometry.animations[1]);
			mesh.userData.currentAnimationAction.setDuration(1).startAt(0).play();

		}

		mesh.translateX(0.0);
		mesh.translateY(300.0);
		mesh.translateZ(0.0);
		mesh.visibility = false;


		mesh.userData.gameObjectType = constants.TYPE_SPIDER;
		mesh.userData.collision = false;

		mesh.userData.name = createName();
		mesh.userData.velocity = new three.Vector3(0.0, 0.0, 0.0);
		mesh.userData.active = false;
		mesh.userData.perpendicularVector = null;
		mesh.userData.isFalling = true;
		mesh.userData.floor = null;
		mesh.userData.wall = null;
		mesh.userData.boundingBox = null;
		mesh.userData.playingAnimation = false;


		mesh.userData.stopAnimation = function(){
			mesh.userData.currentAnimationAction.stop();
			mesh.userData.playingAnimation = false;
		};

		mesh.userData.playAnimation = function(){

			// Return if already playing
			if(mesh.userData.playingAnimation){return;}

			mesh.userData.currentAnimationAction.setDuration(1).startAt(0).play();
			mesh.userData.playingAnimation = true;

		};

		mesh.userData.updateBoundingBox = function(){
			mesh.userData.boundingBox = {
				top: mesh.position.y + constants.CHARACTER_HEIGHT,
				bottom: mesh.position.y,
				left: mesh.position.x - constants.CHARACTER_HALF_WIDTH,
				right: mesh.position.x + constants.CHARACTER_HALF_WIDTH
			}
		};

		mesh.userData.activate = function(uuid){

			mesh.userData.active = true;
			mesh.userData.uuid = uuid;
			mesh.userData.velocity = new three.Vector3(0.0, 0.0, 0.0);
			mesh.userData.perpendicularVector = null;
			mesh.userData.wall = null;
			mesh.userData.boundingBox = null;
			mesh.userData.isFalling = true;
			mesh.visible = true;
			scene.add(mesh);
			spiderDictionary[uuid] = mesh;

		};

		mesh.userData.deactivate = function(){

			console.log("deactivate");
			var uuid = mesh.userData.uuid;

			mesh.userData.active = false;
			mesh.userData.uuid = null;
			mesh.userData.velocity = null;
			mesh.visible = false;
			scene.remove(mesh);
			delete spiderDictionary[uuid];

		};

		mesh.userData.handleMoveInput = function(direction){

//Added dash functionality.  but nested if sucks.
//2 indicates shift key was hit. 1 hit means normal walking
			if(mesh.userData.isFalling){
				mesh.userData.velocity.setX(
					mesh.userData.velocity.x + (direction * constants.CHARACTER_HORIZONTAL_FORCE)
				)
			}
			else if(direction == -2.0){
				mesh.userData.velocity.add(mesh.userData.perpendicularVector.clone()
				.multiplyScalar(constants.CHARACTER_DASH_FORCE));
			}
			else if(direction === -1.0){
				mesh.userData.velocity.add(mesh.userData.perpendicularVector.clone()
				.multiplyScalar(constants.CHARACTER_HORIZONTAL_FORCE));
			}
			else if(direction == 2.0){
				mesh.userData.velocity.add(mesh.userData.perpendicularVector.clone()
				.multiplyScalar(-constants.CHARACTER_DASH_FORCE));
			}
			else{
				mesh.userData.velocity.add(mesh.userData.perpendicularVector.clone()
				.multiplyScalar(-constants.CHARACTER_HORIZONTAL_FORCE));
			}

		};

		mesh.userData.handleFallThroughFloorInput = function(){

			// Only fall through floor when standing still
			if(mesh.userData.isFalling){
				return;
			}

			mesh.userData.isFalling = true;
			mesh.userData.perpendicularVector = null;
			mesh.userData.isFallingThroughFloorCounter = 5;
		};

		mesh.userData.handleJumpInput = function(){


			// Do normal jump if you are on the ground
			if(mesh.userData.perpendicularVector){

				mesh.userData.isFalling = true;
				mesh.userData.perpendicularVector = null;
				mesh.userData.velocity.setY(
					constants.CHARACTER_JUMP_FORCE
				);
			}

			// Commenting out wall jump feature
// 			else if(mesh.userData.wall){ // Do a wall jump if in the air and next to a wall
// 				mesh.userData.isFalling = true;
// 				mesh.userData.perpendicularVector = null;
//
// 				// Jump in opposite direction of wall
// 				var xDif = mesh.position.x - mesh.userData.wall.geometry.vertices[0].x;
// 				var direction = xDif / Math.abs(xDif);
//
// 				// Set wall velocity for wall jump
// 				mesh.userData.velocity.set(
// 					direction * constants.CHARACTER_HORIZONTAL_WALL_JUMP_FORCE,
// 					constants.CHARACTER_JUMP_FORCE,
// 					0.0
// 				);
// 			}

		};

		// All purpose line collision check
		mesh.userData.checkForLine = function(xOrigin, yOrigin, direction, threshold, checkFunction){

			// Check for collision with line collision with ray from origin and direction
			raycaster.linePrecision = 1.0;
			raycaster.set(
				new three.Vector3(
					xOrigin,
					yOrigin,
					0.0
				),
				direction
			);

			// Calculate objects intersecting the picking ray
			var intersectList = raycaster.intersectObjects(scene.children, true);

			// intersectList is sorted with closest first
			while(intersectList.length > 0){
				var intersection = intersectList[0];

				// Skip if not line
				if(intersection.object.userData.lineType === null){
					intersectList.shift();
					continue;
				}

				// Skip if checkFunction fails
				if(!checkFunction(intersection)){
					intersectList.shift();
					continue;
				}

				// Return if wall is close enough
				if(intersection.distance < threshold){
					return intersection;
				}

				intersectList.shift();
			}
		};

		// WALL
		mesh.userData.checkForWall = function(){

			if(mesh.userData.velocity.x === 0.0){
				return;
			}

			// Prepare origin values
			var xDirection = mesh.userData.velocity.x / Math.abs(mesh.userData.velocity.x);
			var direction = new three.Vector3(1.0, 0.0, 0.0).multiplyScalar(xDirection);
			var xOrigin = mesh.position.x + ((constants.CHARACTER_HALF_WIDTH + 2.0) * xDirection);
			var threshold = Math.abs(mesh.userData.velocity.x);
			var wallCheckFunc = function(intersection){
				if(intersection.object.userData.lineType === constants.LINE_TYPE_WALL){
					return true;
				}
				return false;
			};

			// Check for intersection at top and bottom of bounding box
			// Top
			var intersection = mesh.userData.checkForLine(
				xOrigin,
				mesh.userData.boundingBox.top,
				direction,
				threshold,
				wallCheckFunc
			);
			if(!intersection){
				var intersection = mesh.userData.checkForLine(
					xOrigin,
					mesh.position.y + constants.CHARACTER_HALF_HEIGHT,
					direction,
					threshold,
					wallCheckFunc
				);
			}
			// Bottom
			if(!intersection){
				intersection = mesh.userData.checkForLine(
					xOrigin,
					mesh.userData.boundingBox.bottom,
					direction,
					threshold,
					wallCheckFunc
				);
			}

			// If either top or bottom intersections exist, stop player movement
			if(intersection){

				mesh.position.setX(intersection.point.x + ((constants.CHARACTER_HALF_WIDTH + 2.0) * -xDirection));
				mesh.userData.velocity.setX(0.0);

				// Don't stop the player's y movement if jumping/falling
				if(!mesh.userData.isFalling){
					mesh.userData.velocity.setY(0.0);
				}

				mesh.userData.wall = intersection.object;
			}else{
				mesh.userData.wall = null;
			}
		};

		// CEILING
		mesh.userData.checkForCeiling = function(){

			// Skip if the player isn't moving upward
			if(mesh.userData.velocity.y < 0.0){return;}

			// Prepare origin values
			var direction = new three.Vector3(0.0, 1.0, 0.0);
			var threshold = Math.abs(mesh.userData.velocity.y);
			var ceilingCheckFunc = function(intersection){
				if(intersection.object.userData.lineType === constants.LINE_TYPE_HARD_FLOOR){
					return true;
				}
				return false;
			};

			// Check for intersection at left and right of bounding box
			// Left
			var intersection = mesh.userData.checkForLine(
				mesh.userData.boundingBox.left,
				mesh.position.y + constants.CHARACTER_HEIGHT - 2.0,
				direction,
				threshold,
				ceilingCheckFunc
			);
			// Right
			if(!intersection){
				intersection = mesh.userData.checkForLine(
					mesh.userData.boundingBox.right,
					mesh.position.y + constants.CHARACTER_HEIGHT - 2.0,
					direction,
					threshold,
					ceilingCheckFunc
				);
			}

			// If any intersection exists, stop upward velocity
			if(intersection){
				mesh.userData.velocity.setY(0.0);
				mesh.position.setY((intersection.point.y - constants.CHARACTER_HEIGHT) - 2.0);
			}
		};

		// FLOOR
		mesh.userData.checkForNewFloor = function(){

			// Skip check if still inside the bounds of the line
			if(
				mesh.position.x > mesh.userData.floor.userData.leftLimit &&
				mesh.position.x < mesh.userData.floor.userData.rightLimit
			){
				return;
			}

			// Check for collision with blocks
			raycaster.set(
				mesh.position.clone().add(new three.Vector3(0.0, constants.CHARACTER_HEIGHT, 0.0)),
				new three.Vector3(0.0, -1.0, 0.0)
			);

			// Calculate objects intersecting the picking ray
			var intersectList = raycaster.intersectObjects(scene.children, true);

			// Fall if nothing below the player
			if(intersectList.length === 0){
				console.log("nothing below player");
				mesh.userData.isFalling = true;
				mesh.userData.perpendicularVector = null;
			}

			// intersectList is sorted with closest first
			while(intersectList.length > 0){
				var intersection = intersectList[0];

				// Skip if collision not enabled
				if(!intersection.object.userData.collision){
					intersectList.shift();
					continue;
				}

				if(intersection.object === mesh.userData.floor){
					return;
				}

				// If player is far enough off ground to fall one frame, turn falling on
				if(intersection.distance < (constants.CHARACTER_HEIGHT + constants.GRAVITY)){

					// TODO: Make refactored code that sets charactes new surface
					// (this function and the fall function should use it)
					mesh.position.copy(
						new three.Vector3(
							intersection.point.x,
							intersection.point.y + 1.0,
							0.0
						)
					);

					// Get perpendicular of new face
					mesh.userData.perpendicularVector = intersection.object.userData.tangent;

					// Translate speed into new velocity based on new perpendicular
					var speed = mesh.userData.velocity.length();
					var direction = mesh.userData.velocity.x / Math.abs(mesh.userData.velocity.x);
					mesh.userData.velocity = mesh.userData.perpendicularVector.clone().multiplyScalar(speed * -direction);

					// Set new surface
					mesh.userData.floor = intersection.object;
					console.log("setting new surface");

					return;
				}

				intersectList.shift();
			}

			// If nothing close enough to player's feet,
			// start falling once bounding box is completely clear of the surface
			if(
				mesh.userData.boundingBox.left > mesh.userData.floor.userData.rightLimit ||
				mesh.userData.boundingBox.right < mesh.userData.floor.userData.leftLimit
			){
				console.log("Floor too far below player. Falling");
				mesh.userData.isFalling = true;
				mesh.userData.perpendicularVector = null;
				return;
			}


		};

		// FLOOR
		mesh.userData.checkForLandingSurface = function(){

			// Return now if not falling downward
			if(mesh.userData.velocity.y > 0.0){
				return;
			}

			// Prepare origin values
			var direction = mesh.userData.velocity.clone().normalize();
			var threshold = mesh.userData.velocity.length();
			var floorCheckFunc  = function(intersection){

				// Skip soft floors if falling through floor counter is above 0
				if(mesh.userData.isFallingThroughFloorCounter > 0){
					if(intersection.object.userData.lineType){
						if(intersection.object.userData.lineType === constants.LINE_TYPE_SOFT_FLOOR){
							return false;
						}
					}
				}

				// Only check against certain types of lines
				if(
					intersection.object.userData.lineType === constants.LINE_TYPE_HARD_FLOOR ||
					intersection.object.userData.lineType === constants.LINE_TYPE_SOFT_FLOOR
				){
					return true;
				}

				return false;
			};

			// Check for intersection at center
			var intersection = mesh.userData.checkForLine(
				mesh.position.x,
				mesh.position.y + 2.0,
				direction,
				threshold,
				floorCheckFunc
			);

			// If any intersection
			if(intersection){

				// Snap mesh to the intersection point
				mesh.position.copy(
					new three.Vector3(
						intersection.point.x,
						intersection.point.y + 2.0,
						0.0
					)
				);

				// Get tangent of face
				mesh.userData.perpendicularVector = intersection.object.userData.tangent;

				// Stop y velocity
				mesh.userData.velocity.setY(0.0);

				// Translate x velocity into new velocity based on new tangent
				mesh.userData.velocity = mesh.userData.perpendicularVector.clone().multiplyScalar(-mesh.userData.velocity.x);

				mesh.userData.isFalling = false;
				mesh.userData.floor = intersection.object;

			}
			else{
				mesh.userData.checkForLedge();
			}
		};

		mesh.userData.checkForLedge = function(){

			// Return now if not falling downward
			if(mesh.userData.velocity.y > 0.0){
				return;
			}

			// Prepare origin values
			var direction = new three.Vector3(0.0, -1.0, 0.0);
			var threshold = Math.abs(mesh.userData.velocity.y);
			var floorCheckFunc  = function(intersection){

				// Skip soft floors if falling through floor counter is above 0
				if(mesh.userData.isFallingThroughFloorCounter > 0){
					if(intersection.object.userData.lineType){
						if(intersection.object.userData.lineType === constants.LINE_TYPE_SOFT_FLOOR){
							return false;
						}
					}
				}

				// Skip angled surfaces
				if(intersection.object.userData.tangent && intersection.object.userData.tangent.y != 0.0){
					return false;
				}

				// Only check against certain types of lines
				if(
					intersection.object.userData.lineType === constants.LINE_TYPE_HARD_FLOOR ||
					intersection.object.userData.lineType === constants.LINE_TYPE_SOFT_FLOOR
				){
					return true;
				}

				return false;
			};

			// Check for intersection at left and right of bounding box
			// Left
			var intersection = mesh.userData.checkForLine(
				mesh.userData.boundingBox.left,
				mesh.position.y + 2.0,
				direction,
				threshold,
				floorCheckFunc
			);
			// Right
			if(!intersection){
				intersection = mesh.userData.checkForLine(
					mesh.userData.boundingBox.right,
					mesh.position.y + 2.0,
					direction,
					threshold,
					floorCheckFunc
				);
			}

			// If any intersection
			if(intersection){

				// Snap mesh to the intersection point
				mesh.position.copy(
					new three.Vector3(
						mesh.position.x,
						intersection.point.y + 2.0,
						0.0
					)
				);

				// Get tangent of face
				mesh.userData.perpendicularVector = intersection.object.userData.tangent;

				// Stop y velocity
				mesh.userData.velocity.setY(0.0);

				// Translate x velocity into new velocity based on new tangent
				mesh.userData.velocity = mesh.userData.perpendicularVector.clone().multiplyScalar(-mesh.userData.velocity.x);

				mesh.userData.isFalling = false;
				mesh.userData.floor = intersection.object;

			}
		};

		mesh.userData.move = function(){

			mesh.userData.updateBoundingBox();

			if(mesh.userData.perpendicularVector){

				// Surface tangent friction
				mesh.userData.velocity.multiplyScalar(constants.CHARACTER_FRICTION);

				mesh.userData.checkForWall();
				mesh.userData.checkForNewFloor();

				// Set new position
				mesh.position.add(
					mesh.userData.velocity
				)

			}else{

				// Horizontal friction
				mesh.userData.velocity.setX(mesh.userData.velocity.x * constants.CHARACTER_FRICTION);
				mesh.userData.checkForWall();
				mesh.position.setX(mesh.position.x + mesh.userData.velocity.x);
				mesh.userData.updateBoundingBox();

				// Apply gravity
				mesh.userData.velocity.setY(mesh.userData.velocity.y - constants.GRAVITY);
				mesh.userData.checkForLandingSurface();
				mesh.userData.checkForCeiling();

				// Set new position
				mesh.position.setY(mesh.position.y + mesh.userData.velocity.y);

				// Reduce counter
				if(mesh.userData.isFallingThroughFloorCounter > 0){
					mesh.userData.isFallingThroughFloorCounter--;
				}

			}

		};

		return mesh;

	};

	return {
		setSpiderMaterial: function(spiderTexture){

			spiderMaterial = new three.MeshBasicMaterial({
				map: spiderTexture,
				vertexColors: three.FaceColors,
				transparent: true,
				side: three.DoubleSide
			});

		},

		setSpiderGeometry: function(_spiderGeometry){

			spiderGeometry = _spiderGeometry;

		},

		createSpiders: function(scene){
			for(var s = 0; s < constants.NUM_SPIDERS; s++){
				var spider = createSpider(scene);
				// HACK
				spider.scale.set(30.0, 30.0, 30.0);
				spiders.push(spider);
			}
		},

		getSpider: function(index){
			return spiders[index];
		},

		getFreeSpider: function(){
			for(var s in spiders){
				var spider = spiders[s];
				if(!spider.userData.active){
					return spider;
				}
			}
		},

		playMeshAnimations: function(dt){
			for(var s in spiders){
				var spider = spiders[s];

				// Skip inactive spiders
				if(!spider.userData.active){continue;}

				// Update spider positions
				spider.userData.animationMixer.update(dt);
			}
		},

		updateSpiders: function(){
			for(var s in spiders){
				var spider = spiders[s];

				// Skip inactive spiders
				if(!spider.userData.active){continue;}

				// Update spider positions
				spider.userData.move();
			}
		},

		getSpiderBuffer: function(){

			var returnDictionary = {};

			for(var s in spiderDictionary){
				var spider = spiderDictionary[s];

				// Skip inactive spiders
				if(!spider.userData.active){continue;}

				returnDictionary[s] = [
					spider.position.x,
					spider.position.y,
					spider.position.z,
					spider.rotation.x,
					spider.rotation.y,
					spider.rotation.z
				];
			}

			return returnDictionary;
		},

		spiders: spiders,
		spiderDictionary: spiderDictionary
	};
};

module.exports = SpiderFactory;
