// ====================================================================================================
// Imports
// ====================================================================================================


// Node
var async = require("async");
var util = require("util");
var fs = require("fs");

// Third Party
var three = require("three");
var WebSocketServer = require("ws").Server;

// App
var constants = require("./common/constants");
var projectileFactory = require("./projectileFactory.js")();
var spiderFactory = require("./spiderFactory.js")();
var lineFactory = require("./lineFactory.js")();


//====================================================================================================
// WEB SOCKET SERVER
//====================================================================================================


var wss = new WebSocketServer({port: 8080}); 
var clientDict = {};

wss.on("connection", function(ws){

	var sessionId = guid();
	ws.sessionId = sessionId;
	clientDict[sessionId] = ws;

	// Create spider for this client
	var clientSpider = spiderFactory.getFreeSpider();
	if(clientSpider){
		clientSpider.userData.activate(sessionId);
		clientSpider.position.set(0.0, 500.0, 0.0);
	}

	// Wrapper that always stringifies JSON before sending
	ws.sendWrap = function(data){
		ws.send(
			JSON.stringify(data)
		);
	};

	// Send login message along with relevant map and user data
	ws.sendWrap({
		payload: {
			actionId: constants.ACTION_ID_CONNECT,
			sessionId: sessionId,
			spiderBuffer: spiderFactory.getSpiderBuffer()
		}
	});

	// Handle messages recieved from this client
	ws.on("message", function(data, flags){

		try{

			// Parse client requests here
			data = JSON.parse(data);

			// On move
			if(data.payload && data.payload.actionId == constants.ACTION_ID_MOVE){
				var spiderUuid = data.payload.spiderUuid;
                                var spider = spiderFactory.spiderDictionary[spiderUuid];
				spider.userData.handleMoveInput(data.payload.direction);
			}

			// On jump
			if(data.payload && data.payload.actionId == constants.ACTION_ID_JUMP){
				var spiderUuid = data.payload.spiderUuid;
                                var spider = spiderFactory.spiderDictionary[spiderUuid];
				spider.userData.handleJumpInput();
			}

			// On fall through floor
			if(data.payload && data.payload.actionId == constants.ACTION_ID_FALL_THROUGH_FLOOR){
				var spiderUuid = data.payload.spiderUuid;
                                var spider = spiderFactory.spiderDictionary[spiderUuid];
				spider.userData.handleFallThroughFloorInput();
			}

			// On save lines
			if(data.payload && data.payload.actionId == constants.ACTION_ID_SAVE_LINES){
				var lineData = data.payload.lineData;
				var lineDataString = "var LineData = {\n\tdata:";
				lineDataString += util.inspect(lineData);
				lineDataString += "}\nmodule.exports = LineData;";

				fs.writeFile("src/lineData.js", lineDataString, function(err){
					if (err) throw err;
					console.log('It\'s saved!');
				});
				
			}

		}catch(err){
			console.log(util.inspect(err));
		}

	});

	// Handle when a player leaves
	ws.on("close", function(data, flags){
		
		console.log("connection closed");
		
		spiderFactory.spiderDictionary[sessionId].userData.deactivate();
		delete clientDict[sessionId];
		delete ws;
	});

});

wss.broadcast = function broadcast(data){

	wss.clients.forEach(
		function(ws){
			try{
				ws.sendWrap(data);
			}catch(err){
				// TODO: handle code for when client is closed mid-broadcast
				console.log(util.inspect(err));
			}
		}
	);

};


//====================================================================================================
// GAME DATA
//====================================================================================================


// Initialize scene
// Make map
// Buffer N spiders (Buffered but not active/visible until players join)
// Start server
// Start game simulation loop

var scene = null;
var clock = new three.Clock();
var lastTime = new Date().getTime();

async.series(
	[
		function(callback){
			// Initialize scene
			scene = new three.Scene();

			lineFactory.createLines(scene);

			callback(null, "initializeScene");
		},
		function(callback){
			// Create spiders
			spiderFactory.createSpiders(scene);
			callback(null, "createSpiders");
		},
		function(callback){
			// Create projectiles
			projectileFactory.createProjectiles(scene);
			callback(null, "createProjectiles");
		}
	],
	function(err, result){

		gameLoop();

	}
);


//====================================================================================================
// GAME LOOP
//====================================================================================================


function gameLoop(){

	// Run this function 30 times a second (1000ms / 30frames)
	setTimeout(
		gameLoop,
		33.3
	);

	spiderFactory.updateSpiders();
	projectileFactory.updateProjectiles();

	// Send game state to all clients
	wss.broadcast({
		payload: {
			actionId: constants.ACTION_ID_SNAPSHOT,
			spiderBuffer: spiderFactory.getSpiderBuffer(),
			projectileBuffer: projectileFactory.getProjectileBuffer()
		}
	});

}


//====================================================================================================
// HELPER FUNCTIONS
//====================================================================================================


function guid(){
        function s4(){
                return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
        }
  
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};


//====================================================================================================
// EOF
//====================================================================================================
