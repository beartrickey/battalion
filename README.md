# TROSS #

### HOW TO ###
#### RUN STATIC SERVER (Mac/Linux)####
* Install and startup docker
* Run `./launch_static_server.sh`

#### RUN STATIC SERVER (Windows)####
* Install and startup docker
* Run `browserify src/client.js -o static/bundle.js`
* Run `docker stop static_server`
* Run `docker rm static_server`
* Run `docker build -t static_server -f static_server.dockerfile .`
* Run `docker run --name static_server -d -p 8181:8181 static_server`

#### RUN GAME SERVER ####
* Run `node src/server.js`

#### PLAY THE GAME LOCALLY ####
* Run static and game servers
* In browser, visit `http://localhost:8181/?serverAddress=[INSERT GAME SERVER ADDRESS HERE]`
* In [INSERT GAME SERVER ADDRESS HERE], use 'localhost:8080' or 'ursablogger.com:8080'

### KEY THEMES ###
* Bots jump around and shoot at eachother in realtime


#### Markdown Help ####
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
