
// JSON == JavaScript Object Notation
a = {
	"foo": "bar",
	"wizzle": "wuzzle",
	"qwe ert": 1
};
console.log(a["foo"]);
console.log(a.foo);

// Functions are objects too
var B = function(){

	this.foo = "bar";
	this.wizzle = "wuzzle";
}

b = new B();
console.log(b.foo);
console.log(b["foo"]);


