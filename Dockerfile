FROM ubuntu:latest


#====================================================================================================
# Apt-get
#====================================================================================================


RUN \
apt-get update && \
apt-get install -y git && \
apt-get install -y nodejs && \
apt-get install -y npm && \
apt-get install -y vim && \
apt-get install -y nginx


#====================================================================================================
# Git
#====================================================================================================


# Setup app directory
COPY cache_bust /cache_bust
RUN mkdir -p /opt/Tross
RUN git clone https://beartrickey@bitbucket.org/beartrickey/tross-tactics.git /opt/Tross


#====================================================================================================
# Node.js
#====================================================================================================


# Set node binary symlink (in case other apps call node instead of nodejs)
RUN ln -s /usr/bin/nodejs /usr/bin/node

# Install npm packages
WORKDIR /opt/Tross
RUN npm install

# Bundle all client source code into static folder
RUN /opt/Tross/node_modules/.bin/browserify /opt/Tross/src/client.js -o /opt/Tross/static/bundle.js


#====================================================================================================
# NGINX
#====================================================================================================


# Copy NGINX configurations
RUN cp /opt/Tross/nginx.conf /etc/nginx/nginx.conf

# Start NGINX in foreground
CMD ["nginx", "-g", "daemon off;"]

