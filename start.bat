docker stop static_server
pause
docker rm static_server
pause
docker build -t static_server -f static_server.dockerfile .
pause
docker run --name static_server -d -p 8181:8181 static_server
pause
node src/server.js
